s = tf('s');

Td1 = 0.1;
Tdc = sqrt(10/25);
Td2 = 2;

G_ol1 = (1+s*Td1)*10/s^2;
G_olc = (1+s*Tdc)*10/s^2;
G_ol2 = (1+s*Td2)*10/s^2;

%rlocus(G_ol1);
%rlocus(G_olc);
rlocus(G_ol2);

grid();

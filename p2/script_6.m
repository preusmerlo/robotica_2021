s = tf('s');
G_ol = 10/s^2;

K1 = 0.1;
K2 = 1;
K3 = 10;

G_cl1 = feedback(K1 * G_ol,1);
G_cl2 = feedback(K2 * G_ol,1);
G_cl3 = feedback(K3 * G_ol,1);


[y1,x1] = step(G_cl1);
[y2,x2] = step(G_cl2);
[y3,x3] = step(G_cl3);

hold on
p = plot(x1,y1,'-x');
p(1).LineWidth = 1.5;
p = plot(x2,y2,'-.');
p(1).LineWidth = 1.5;
p = plot(x3,y3,'-o');
p(1).LineWidth = 1.5;

grid();

xlim([0 8])
ylim([0 2])

xlabel('Tiempo');
ylabel('Amplitud');

legend('K = 0.1','K = 1', 'K = 10');
hold off
s = tf('s');

time = 10;          % Tiempo a graficar

%K  = 0.1;          % Definir ganancia
%K  = 1;
%K  = 10;

%Td = 0.1;          % Definir tiempo derivativo
%Td = sqrt(10/25);
%Td = 2;

G_ol = K*(1+s*Td)*10/s^2;

G_cl = feedback(G_ol,1);

[y,x] = step(G_cl,time);
p = plot(x,y,'-r');

xlim([0 time]);
ylim([0 2]);

xlabel('Tiempo');
ylabel('Amplitud');
p(1).LineWidth = 3;
legend(sprintf('K = %.1f y Td = %.2f', K,Td));
grid();

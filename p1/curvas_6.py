import numpy as np
from matplotlib import pyplot as plt

# Archivo log a graficar
file = 'log_6.txt'

# Levanta la tabla en listas
tiempo      = np.genfromtxt(file, usecols = 0, dtype=float) 
pos_x       = np.genfromtxt(file, usecols = 1, dtype=float)   
pos_y       = np.genfromtxt(file, usecols = 2, dtype=float)   
orientacion = np.genfromtxt(file, usecols = 3, dtype=float)   
v_lineal    = np.genfromtxt(file, usecols = 4, dtype=float)   
v_angular   = np.genfromtxt(file, usecols = 5, dtype=float)

##### Configurables #####
index_min = 1                # Limite inferior de los datos a graficar
index_max = 2000               # Limite superior de los datos a graficar
index_a   = 400                # Indice del primer  punto a graficar
index_b   = 1900                # Indice del segundo punto a graficar

offset = 50
bbox = dict(boxstyle ="round",ec = "0",fc ="0.7")
arrowprops = dict(facecolor='grey',arrowstyle = "->",connectionstyle = "arc,angleA=0,armA=50,rad=10")

##### Plot de tryectoria #####
plt.subplot(2, 2, 1)
plt.plot(pos_x,pos_y,'-b')
plt.title("Trayectoria")
plt.xlabel('X [m]')
plt.ylabel('Y [m]')
plt.grid()

# Puento en la grafica
plt.scatter(pos_x[index_a], pos_y[index_a], color = 'b')

plt.annotate('(%.1f, %.1f)'%(pos_x[index_a], pos_y[index_a]),
            (pos_x[index_a], pos_y[index_a]), xytext =(-2*offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)

##### Plot posicion en funcion de tiempo #####
plt.subplot(2, 2, 2)
plt.plot(tiempo[index_min:index_max],pos_x[index_min:index_max],'-b',label = 'Pos. en X')
plt.plot(tiempo[index_min:index_max],pos_y[index_min:index_max],'-r',label = 'Pos. en X')
plt.title("Posición en X e Y")
plt.xlabel('Tiempo')
plt.ylabel('Posición')
plt.legend()
plt.grid()

# Puento en la grafica
plt.scatter(tiempo[index_a], pos_x[index_a], color = 'b')

plt.annotate('(%.1f, %.1f)'%(tiempo[index_a], pos_x[index_a]),
            (tiempo[index_a], pos_x[index_a]), xytext =(-1.8 * offset, - 0.2 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)     

plt.scatter(tiempo[index_a], pos_y[index_a], color = 'r')

plt.annotate('(%.1f, %.1f)'%(tiempo[index_a], pos_y[index_a]),
            (tiempo[index_a], pos_y[index_a]), xytext =(-1.8*offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)
       

##### Plot de tryectoria #####
plt.subplot(2, 2, 3)
plt.plot(pos_x,pos_y,'-b')
plt.title("Trayectoria")
plt.xlabel('X [m]')
plt.ylabel('Y [m]')
plt.grid()


# Puento en la grafica
plt.scatter(pos_x[index_b], pos_y[index_b], color = 'b')

plt.annotate('(%.1f, %.1f)'%(pos_x[index_b], pos_y[index_b]),
            (pos_x[index_b], pos_y[index_b]), xytext =(-2*offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)

##### Plot posicion en funcion de tiempo #####
plt.subplot(2, 2, 4)
plt.plot(tiempo[index_min:index_max],pos_x[index_min:index_max],'-b',label = 'Pos. en X')
plt.plot(tiempo[index_min:index_max],pos_y[index_min:index_max],'-r',label = 'Pos. en X')
plt.title("Posición en X e Y")
plt.xlabel('Tiempo')
plt.ylabel('Posición')
plt.legend()
plt.grid()

# Puento en la grafica
plt.scatter(tiempo[index_b], pos_x[index_b], color = 'b')

plt.annotate('(%.1f, %.1f)'%(tiempo[index_b], pos_x[index_b]),
            (tiempo[index_b], pos_x[index_b]), xytext =(-1.8 * offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)     

plt.scatter(tiempo[index_b], pos_y[index_b], color = 'r')

plt.annotate('(%.1f, %.1f)'%(tiempo[index_b], pos_y[index_b]),
            (tiempo[index_b], pos_y[index_b]), xytext =(-1.8*offset,  -0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)
       

plt.show()
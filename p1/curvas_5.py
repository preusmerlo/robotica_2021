import numpy as np
from matplotlib import pyplot as plt

##### Configurables #####
file  = 'log_5.txt'         # Archivo log a graficar
i_min = 1                   # Limite inferior de los datos a graficar
i_max = 4460                # Limite superior de los datos a graficar

##### Levanta la tabla en listas #####
tiempo      = np.genfromtxt(file, usecols = 0, dtype = float) 
pos_x       = np.genfromtxt(file, usecols = 1, dtype = float)   
pos_y       = np.genfromtxt(file, usecols = 2, dtype = float)   
orientacion = np.genfromtxt(file, usecols = 3, dtype = float)   
v_lin       = np.genfromtxt(file, usecols = 4, dtype = float)   
v_ang       = np.genfromtxt(file, usecols = 5, dtype = float)

##### Plot velocidad lineal y velocidad angular #####
plt.subplot(2, 2, 1)
plt.plot(tiempo[i_min:i_max],v_lin[i_min:i_max],'-b',label='Vel. Lineal' )
plt.plot(tiempo[i_min:i_max],v_ang[i_min:i_max],'-r',label='Vel. Angular')
plt.title("Velocidad Lineal y Angular")
plt.xlabel('Tiempo [s]')
plt.ylabel('Velocidad')
plt.legend()
plt.grid()

##### Plot posicion en funcion de tiempo #####
plt.subplot(2, 2, 2)
plt.plot(tiempo[i_min:i_max],pos_x[i_min:i_max],'-b',label = 'Pos. en X')
plt.plot(tiempo[i_min:i_max],pos_y[i_min:i_max],'-r',label = 'Pos. en Y')
plt.title("Posición en X e Y")
plt.xlabel('Tiempo [s]')
plt.ylabel('Posición')
plt.legend()
plt.grid()

##### Plot de tryectoria #####
plt.subplot(2, 2, 3)
plt.plot(pos_x,pos_y,'-b')
plt.title("Camino recorrido")
plt.xlabel('X [m]')
plt.ylabel('Y [m]')
plt.grid()

##### Plot de orientacion #####
plt.subplot(2, 2, 4)
plt.plot(tiempo[i_min:i_max],orientacion[i_min:i_max],'-r')
plt.title("Orientación")
plt.xlabel('Tiempo [s]')
plt.ylabel('Orientacion [rad]')
plt.grid()

plt.show()

#! /bin/python3

import numpy as np
import matplotlib.pyplot as plt

##### Configurables #####
path      = 'data_1/'
# path      = 'data_2/'
scan_file = path + 'scan.txt' 
odom_file = path + 'odom.txt' 

##### Levanta la tabla en listas #####
time_scan = np.genfromtxt(scan_file, usecols = 0, dtype = float)
time_odom = np.genfromtxt(odom_file, usecols = 0, dtype = float)

##### Iguala frecuencias de muestreo #####
new_time_odom = []
for i in range(len(time_scan)):
    new_time_odom.append(time_odom[np.abs(time_odom - time_scan[i]).argmin()])

##### Plot #####
x = np.arange(0,len(new_time_odom),1)
f = plt.figure()
f.set_figwidth(6)
f.set_figheight(6)
plt.plot(x,time_scan,'b-',linewidth=8,label='time_scan')
plt.plot(x,new_time_odom,'r-.',linewidth=8,label='short_time_odom')
plt.xlabel('Muestra')
plt.ylabel('Tiempo [s]')
plt.grid()
plt.legend()
plt.savefig('img/ej7.pdf',format='pdf')
# plt.show()

#! /bin/python3

import numpy as np
import matplotlib.pyplot as plt

##### Configurables #####
path      = 'data_1/'
# path      = 'data_2/'
file = path + 'scan.txt' 

##### Levanta la tabla en listas #####
laser = np.genfromtxt(file, usecols = np.arange(1,361), dtype=float)
time  = np.genfromtxt(file, usecols = 0, dtype=float)


##### Graficas #####
f = plt.figure()
f.set_figwidth(6)
f.set_figheight(6)

pos  = [0,200,400,500]
rads = np.arange(0,(2*np.pi),(2*np.pi)/360)

for i in range(len(pos)):
    print(time[pos[i]])
    plt.axes(projection = 'polar')
    plt.axes(projection = 'polar')
    plt.plot(rads,laser[pos[i]],'r.')
    plt.xlabel(r'p [deg]' , fontsize = 16, loc = 'right')
    plt.ylabel(r'r [m]' , fontsize = 16, loc = 'top', rotation = 90)
    plt.savefig(f'img/laser_pos_{str(pos[i])}.pdf',format='pdf')






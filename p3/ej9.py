#! /bin/python3

import numpy as np
import matplotlib.pyplot as plt


##### Configurables #####
path      = 'data_1/'
# path      = 'data_2/'
scan_file = path + 'scan.txt'
odom_file = path + 'odom.txt' 

##### Levanta la tabla en listas #####
time_scan = np.genfromtxt(scan_file, usecols = 0, dtype = float)
time_odom = np.genfromtxt(odom_file, usecols = 0, dtype = float)
scan      = np.genfromtxt(scan_file, usecols = np.arange(1,361), dtype=float)
odom      = np.genfromtxt(odom_file, usecols = (1,2,3) , dtype=float)

##### Iguala frecuencias de muestreo #####
short_time_odom = []
short_odom      = []

for i in range(len(time_scan)):
    idx = np.abs(time_odom - time_scan[i]).argmin()
    short_time_odom.append(time_odom[idx])
    short_odom.append(odom[idx])

short_odom = np.array(short_odom)

##### Pasaje a polares #####
scan_x  = np.zeros((np.size(scan,axis = 0), np.size(scan,axis = 1)))
scan_y  = np.zeros((np.size(scan,axis = 0), np.size(scan,axis = 1)))

for i in range(np.size(scan,axis=0)):
    for j in range(np.size(scan,axis=1)):
        if scan[i][j] == np.inf:
            scan_x[i][j] = np.inf
            scan_y[i][j] = np.inf
        else:
            scan_x[i][j] = scan[i][j] * np.cos( j*np.pi/180 + short_odom[i][2] ) + short_odom[i][0]
            scan_y[i][j] = scan[i][j] * np.sin( j*np.pi/180 + short_odom[i][2] ) + short_odom[i][1]

##### Plot Mapa #####
f = plt.figure()
f.set_figwidth(6)
f.set_figheight(6)
plt.xlim( -3 , 3 )
plt.ylim( -3 , 3 )
plt.grid()

step = 60
for i in range(0,len(scan_x[:][0]),step):
    if i == 0:
        plt.plot(scan_x[i][:],scan_y[i][:], '.',color='red',label='Mapa')
    else:
        plt.plot(scan_x[i][:],scan_y[i][:], '.',color='red')    
    


##### Plot Recorrido #####
plt.plot(odom[: ,0],odom[ :,1],'--',color='blue', label='Camino')
plt.plot(odom[0 ,0],odom[ 0,1],'o' ,color='blue', label='Inicio')
plt.plot(odom[-1,0],odom[-1,1],'^' ,color='blue', label='Fin')
plt.xlabel(r'x [m]' , fontsize = 16, loc = 'right')
plt.ylabel(r'y [m]' , fontsize = 16, loc = 'top')
plt.legend()
plt.savefig('img/ej9.pdf',format='pdf')
# plt.show()
